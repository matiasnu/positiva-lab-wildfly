FROM jboss/wildfly:8.2.1.Final

# adds a management user with the following credentials: admin:admin
# if you want to add an application user, use the '-a' option
RUN /opt/jboss/wildfly/bin/add-user.sh admin admin --silent

# Add custom configuration file
ADD config/standalone-full.xml /opt/jboss/wildfly/standalone/configuration/

# Add war archives to deployments
ADD deployments/ /opt/jboss/wildfly/standalone/deployments/

# Add certificates for ssl
ADD config/cert_conexia.com.co.jks /opt/jboss/wildfly/
ADD config/conexia2019.jks /opt/jboss/wildfly/

# Add jconn module
ADD modules/ /opt/jboss/wildfly/modules

# JBoss ports
EXPOSE 8080 9990 8009

CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "-bmanagement", "0.0.0.0", "-Djboss.server.default.config=standalone-full.xml"]