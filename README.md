# Conexia Positiva WildFly

## Information
WildFly Version: 8.2.1-Final

## How to Use
You can lift the wildfly with the following command:

`
    $ docker-compose up -d
`

By default, in addition to building the application server, the docker contains the cas.

## Add services
If you want to add services for the wildfly to lift, what you have to do is copy the .war files, which are generated in the target folder when compiling, to the deployments folder and rebuild the image.
Run:

`
    $ docker-compose up --build -d
`